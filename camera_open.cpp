#include"camera_open.h"
#include"SI_sensor.h"
#include"SI_errors.h"
#include<windows.h>
#include <iostream>
#include <stdio.h>

#define LICENSE_PATH L"C:/Program Files (x86)/Specim/SDKs/SpecSensor/SpecSensor SDK.lic"

bool camera::open()
{
    SI_H g_hDevice=0;

    //create variables

    int nError =siNoError;
    int nDeviceIndex=-1;
    SI_U8* pFramebuffer=0;
    SI_64 nBuffersize=0;
    SI_64 nFramesize=0;
    SI_64 nFramenumber=0;

    //LOAD SPECSENSOR
    std::cout <<(L"LOADING SPECSENSOR") << std::endl;
    SI_CHK(SI_Load(LICENSE_PATH));


    //OPEN CAMERA HANDLE

    SI_CHK(SI_Open(nDeviceIndex,&g_hDevice));
    SI_CHK(SI_Command(g_hDevice,L"Intialise"));

    //create buffer
    SI_CHK(SI_SetFloat(g_hDevice,L"camera.Framerate",25.0));
    SI_CHK(SI_SetFloat(g_hDevice,L"camera.Exposuretime",3.0));

    //create buffer to receive data

    SI_CHK(SI_GetInt(g_hDevice,L"camera.image.sizebytes",&nBuffersize));
    SI_CHK(SI_CreateBuffer(g_hDevice, nBuffersize, (void**)&pFramebuffer));



    // Starts the acquisition, acquires 100 frames and stops the acquisition
    SI_CHK(SI_Command(g_hDevice, L"Acquisition.Start"));

    for(int n = 0; n < 100; n++)
    {
        SI_CHK(SI_Wait(g_hDevice, pFramebuffer, &nFramesize, &nFramenumber, 1000));
        // Do something interesting with the frame pointer (pFrameBuffer)
        wprintf(L"Frame number: %d\n", nFramenumber);
    }

    SI_CHK(SI_Command(g_hDevice, L"Acquisition.Stop"));


Error:
    if (SI_FAILED(nError))
    {
        char szInput[256] = "";
        wprintf(L"An error occurred: %s\n", SI_GetErrorString(nError));
        wprintf(L"Enter a character and press enter to exit\n");
        scanf("%s", &szInput, 255);

    }
    else
    {
        char szInput[256] = "";
        wprintf(L"It all went well!\n");
        wprintf(L"Enter a character and press enter to exit\n");
        scanf("%s", &szInput, 255);
    }

    // Cleanups the buffer, closes the camera and unloads SpecSensor
    SI_DisposeBuffer(g_hDevice, pFramebuffer);
    SI_Close(g_hDevice);
    SI_Unload();

    return SI_FAILED(nError) ? -1 : 0;
}
