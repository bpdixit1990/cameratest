#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "camera_open.h"

#include "SI_sensor.h"
#include "SI_errors.h"

#define LICENSE_PATH L"C:/Program Files (x86)/Specim/SDKs/SpecSensor/SpecSensor SDK.lic"

int SI_IMPEXP_CONV onDataCallback(SI_U8* _pBuffer, SI_64 _nFrameSize, SI_64 _nFrameNumber, void* _pContext);

int SI_IMPEXP_CONV onDataCallback(SI_U8* _pBuffer, SI_64 _nFrameSize, SI_64 _nFrameNumber, void* _pContext)
{
    //wprintf(L"%d ", _nFrameNumber);
    // SOME WORK
    return 0;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //camera* camera = new camera();
    //camera->open();

    SI_H g_hDevice = 0;

    int nError = siNoError;
    int nDeviceIndex = 0;

    SI_CHK(SI_Load(LICENSE_PATH));
    SI_CHK(SI_Open(nDeviceIndex, &g_hDevice));
    SI_CHK(SI_Command(g_hDevice, L"Initialize"));
    SI_CHK(SI_RegisterDataCallback(g_hDevice, onDataCallback, 0));

    Error:
        if (SI_FAILED(nError))
        {
            wprintf(L"An error occurred: %d\n", SI_GetErrorString(nError));
        }

        SI_Close(g_hDevice);
        SI_Unload();


}

MainWindow::~MainWindow()
{
    delete ui;
}
;


